BIN := ~/.local/bin/hexpy

.PHONY: build
build:
	docker build -t hexpy:latest .

.PHONY: unregister
unregister:
	test -f ${BIN} && rm ${BIN} || true

.PHONY: register
register: unregister ${BIN}

${BIN}:
	docker run --rm hexpy:latest script > ${BIN} && \
		chmod u+x ${BIN}
